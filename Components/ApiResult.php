<?php
namespace Lynx\ApiBundle\Components;

class ApiResult {
    private $totalRegistros = 0;
    /**
     * @return mixed
     */
    public function getRegistros()
    {
        return $this->registros;
    }
    /**
     * @param mixed $registros
     */
    public function setRegistros($registros)
    {
        $this->registros = $registros;
    }
    private $registros;
    /**
     * @return int
     */
    public function getTotalRegistros()
    {
        return $this->totalRegistros;
    }
    /**
     * @param int $totalRegistros
     */
    public function setTotalRegistros($totalRegistros = 0)
    {
        $this->totalRegistros = $totalRegistros;
    }
    private $numeroPaginas;
    /**
     * @return mixed
     */
    public function getNumeroPaginas()
    {
        return $this->numeroPaginas;
    }
    /**
     * @param mixed $numeroPaginas
     */
    public function setNumeroPaginas($numeroPaginas)
    {
        $this->numeroPaginas = $numeroPaginas;
    }
    private $paginaActual;
    /**
     * @return mixed
     */
    public function getPaginaActual()
    {
        return $this->paginaActual;
    }
    /**
     * @param mixed $paginaActual
     */
    public function setPaginaActual($paginaActual)
    {
        $this->paginaActual = $paginaActual;
    }
} 