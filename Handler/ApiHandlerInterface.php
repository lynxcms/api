<?php

namespace Lynx\ApiBundle\Handler;


interface ApiHandlerInterface
{
    /**
     * Recupera los datos de un registro por su id
     * @api
     * @param int $id
     * @return Interface
     */
    public function get($id);
    
    /**
     * Recupera todos los registros
     * @api
     * @return Interface
     */
    public function getAll($camposOrdenables, $camposSeleccionables, $camposConsultables,$camposFiltrables);
    
    /**
     * Crea un nuevo registro
     * @api
     * @param array $parameters
     * @return Interface
     */
    public function post(array $parameters);
    
    
 }
