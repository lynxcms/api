<?php

namespace Lynx\ApiBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
//use Symfony\Component\Security\Core\User\User;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManager;

class ApiKeyUserProvider implements UserProviderInterface
{
     
    protected $em;
    
    public function __construct(EntityManager $entityManager){
            $this->em = $entityManager;
}

    public function getUsernameForApiKey($apiKey)
    {
        
         $user = $this->em->getRepository('LynxBundle:User')->findByToken($apiKey);
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        //$username = "admin";
         if (!$user) {
            throw new BadCredentialsException();
            
            // or to just skip api key authentication
            // return null;
        }
         $username = $user[0]->getUsername();

        return $username;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('LynxBundle:User')->findByUsername($username);
        return $user;
        /*return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_API')
        );*/
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}