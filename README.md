INSTALACION
============

Paso 1: Descargar el Bundle
---------------------------

Abra una consola, entre en la raíz de su proyecto y ejecute la siguiente línea:

```console
$ composer require <package-name> "~1"
```


Paso 2: Habilite el Bundle
-------------------------

Luego, habilite el bundle agregando el registro del bundle en el archivo `app/AppKernel.php` del proyecto:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Lynx\ApiBundle\LynxApiBundle(),
        );

        // ...
    }

    // ...
}
```

Paso 3: Habilite el Bundle
-------------------------