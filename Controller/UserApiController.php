<?php

namespace Lynx\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Lynx\ApiBundle\Form\UserType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Date;
use Lynx\ApiBundle\Exception\InvalidFormException;

class UserApiController extends FOSRestController {

    /**
     * Retorna los datos de un Usuario
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Retorna los datos de un usuario",
     *   output = "LynxBundle\Entity\User",
     *   statusCodes = {
     *     200 = "Retorna cuando es satisfactorio el resultado",
     *     404 = "Retorna cuando no exite el recurso"
     *   }
     * )
     * @Annotations\QueryParam(name="id", requirements="\d+", nullable=false, description="Identificador unico de un usuario")
     *
     * @param Request $request the request object
     * @param int $id      Id User
     * @return array
     * @View()
     * @throws NotFoundHttpException when page not exist
     * 
     * @Route("/usuario/{id}")
     */
    public function getUserAction($id) {

        
        if (!($data = $this->container->get('user.api.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        
        $datos = $this->container->get('api.respuestas')->Contenido($data, 10, 2);

        return $datos;
    }

    /**
     * Retorna Todos los usuarios
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Retorna todos los usuarios",
     *   output = "LynxBundle\Entity\User",
     *   statusCodes = {
     *     200 = "Retorna cuando es satisfactorio el resultado",
     *     404 = "Retorna cuando no exite el recurso"
     *   },
     *  parameters={
     *       {"name"="fields", "dataType"="list[string]", "required"=false, "format"="fields:valor1,valor2,valor3...", "description"="Indica los campos a devolver. Campos permitidos: 'name', 'username', 'id'"},
     *       {"name"="q", "dataType"="string", "required"=false, "format"="q:campo",  "description"="Busca en los campos pre-establecidos el valor enviado"},
     *       { "name"="per_page", "required"=false, "dataType"="integer", "format"="per_page:2",  "description"="Indica la cantidad de registros por página"},
     *       { "name"="page", "required"=false, "dataType"="integer",  "format"="page:2", "description"="Indica la página de resultados"},
     *       { "name"="sort", "required"=false, "dataType"="list[string]", "format"="campo:ASC|DESC, campo:ASC|DESC", "description"="Indica por que campo(s) se ordenara el resultado y su direccion. Campos permitidos: 'id','username','name','is_active','datetime_last_conection' "},
     *       { "name"="name", "required"=false, "dataType"="flat[string]", "format"="name:valor", "description"="Busca registros con el name especificado"},
     *       { "name"="username", "required"=false, "dataType"="flat[string]", "format"="username:valor", "description"="Busca registros con el username especificado"},
     *       { "name"="id", "required"=false, "dataType"="list[string]", "format"="id:valor1,valor2,valor3...", "description"="Busca registros con el/los id especificado(S)"},
     *       { "name"="datetimeLastConection", "required"=false, "dataType"="range[date]", "format"="datetimeLastConection: min,max", "description"="Busca registros que coincidan con el intervalo especificado"},   
     *   }
     * )
     *
     * @return array
     * @View()
     * @throws NotFoundHttpException when page not exist
     * 
     * @Route("/")
     */
    public function getUsersAction() {
        
        $camposOrdenables = array('id','username','name','is_active','datetime_last_conection');
        $camposSeleccionables = array('name', 'username', 'id', 'is_active', 'datetime_last_conection');
        $camposConsultables = array('name', 'username', 'id');
        $camposFiltrables = ([
            'name' => ['style' => 'flat', 'validaciones' => [ new Length(['min'=>3, 'max'=>'50']) ]],
            'username' => ['style' => 'flat', 'validaciones' => [ new Length(['min'=>3, 'max'=>'50']) ]],
            'id' => ['style' => 'list', 'validaciones' =>[new GreaterThan(['value'=>0]) ]],
            'datetimeLastConection' => ['style' => 'range', 'validaciones' => [ new Date(), new GreaterThan(['value'=>0])]],
        ]);
        $data = $this->container->get('user.api.handler')->getAll($camposOrdenables, $camposSeleccionables, $camposConsultables, $camposFiltrables);
        
        return $this->container->get('api.respuestas')->Contenido($data->getRegistros(), $data->getTotalRegistros(), $data->getNumeroPaginas(), $data->getPaginaActual() );
    }

    /**
     * Crear un usuario desde el Form
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Crear un usuario desde el Form",
     *   input = "Lynx\ApiBundle\Form\UserType",
     *   statusCodes = {
     *     200 = "Retorna cuando es satisfactorio el resultado",
     *     400 = "Retorna cuando el formulario tiene error",
     *     401 = "Retorna cuando no esta autenticado",
     *     403 = "Retorna cuando no tiene permisos"
     *   }
     * )
     *
     * @View()
     * @Route("/usuario")
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postUserAction(Request $request) {
        try {
            $newPage = $this->container->get('user.api.handler')->post(
                    $request->request->all()
            );

            $routeOptions = array(
                'id' => $newPage->getId(),
                'msg' => 'success'
            );
            return $routeOptions;
            //return $this->routeRedirectView('api_1_post_mac_address', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

}
